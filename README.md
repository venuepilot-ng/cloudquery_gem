# CloudqueryGem

CloudqueryGem is a Ruby gem that provides a seamless integration with the Cloudquery tool, 
allowing you to sync data between different sources and destinations.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'cloudquery_gem'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install cloudquery_gem

## Usage

CloudqueryGem allows you to easily sync and manage data using the Cloudquery tool through Ruby. Follow these steps to get started:

1. Install the gem by adding it to your Gemfile:
```ruby
gem 'cloudquery_gem', git: 'https://gitlab.com/venuepilot-ng/cloudquery_gem.git'
```

2. Use the CloudqueryGem.sync_data method to sync data between sources and destinations:
```ruby
source_path = 'path/to/source.yml'
destination_path = 'path/to/destination.yml'

CloudqueryGem.sync_data(source_path, destination_path)
```
