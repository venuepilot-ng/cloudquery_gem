# frozen_string_literal: true

require_relative "cloudquery_gem/version"
require 'logger'

module CloudqueryGem
  class CloudqueryError < StandardError; end

  PLATFORM = RUBY_PLATFORM.downcase

  # Set the binary path based on the platform
  if PLATFORM.include?("darwin")
    CLOUDQUERY_BINARY = File.expand_path("../../bin/cloudquery_darwin_amd64", __FILE__)
  elsif PLATFORM.include?("linux")
    CLOUDQUERY_BINARY = File.expand_path("../../bin/cloudquery_linux_amd64", __FILE__)
  else
    raise CloudqueryError, "Unsupported platform: #{PLATFORM}"
  end

  LOGGER = Logger.new(STDOUT)

  def self.sync_data(source_path, destination_path)
    unless File.exist?(CLOUDQUERY_BINARY)
      raise CloudqueryError, "Cloudquery binary not found. Please make sure it's located at #{CLOUDQUERY_BINARY}"
    end

    unless File.exist?(source_path)
      raise CloudqueryError, "Source data file not found. Please check the path: #{source_path}"
    end

    unless File.exist?(destination_path)
      raise CloudqueryError, "Destination data file not found. Please check the path: #{destination_path}"
    end

    LOGGER.info "Starting data sync..."
    command = "#{CLOUDQUERY_BINARY} sync #{source_path} #{destination_path}"
    LOGGER.info "Executing command: #{command}"

    IO.popen(command, err: [:child, :out]) do |io|
      io.each_line do |line|
        next if line.include?('Syncing resources...')
        LOGGER.info line.chomp
      end
    end

    success = $?.success?

    if success
      LOGGER.info "Data sync completed successfully!"
    else
      raise CloudqueryError, "Cloudquery sync command failed with exit code #{$?.exitstatus}"
    end

    success
  rescue CloudqueryError => e
    puts "Error: #{e.message}"
  end
end
