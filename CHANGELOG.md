## [Unreleased]

## [0.1.4] - 2023-11-13
### Changed
-  Sync_data logs verbosity fix

## [0.1.3] - 2023-10-05
### Changed
- Fix for sync_data

## [0.1.2] - 2023-08-21
### Changed
- Added support for different platforms by using separate binaries
- Updated gemspec

## [0.1.1] - 2023-07-28
### Changed
- Update cloudquery binary to linux_amd_64
- Added gemspec

## [0.1.0] - 2023-07-27
- Initial release
